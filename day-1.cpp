#include <iostream>

using namespace std;

void init(int*, int);
void sort(int*, int);
void swap(int&, int&);
void display(int*, int);
bool comparison(int, int);
bool search(int*, int, int*);
bool containsLetter(int);
int* allocate(int n);
int* removeElement(int*, int, int*);
int* removeEquals(int* a, int& n);
int* copyBySymbols(int*, int, int&);
int* removeBySymbols(int*, int&, int*, int);

int main () 
{
	int n = 5;
	int* array = allocate(n);

	init(array, n);
	array = removeEquals(array, n);

	int l = 0;
	int* arraySymbols = copyBySymbols(array, n, l);
	array = removeBySymbols(array, n, arraySymbols, l);

	sort(array, n);	
	display(array, n);

	delete[] array;

	return 0;
}

int* allocate(int n)
{
	if (n <= 0)
	{
		return nullptr;
	}

	return new int[n];
}

void init(int* a, int n) {

	for (int* p = a; p < a + n; p++)
	{
		*p = rand() % 100 - 40;
	}
}

void display(int* a, int n)
{

	for (int* p = a; p < a + n; p++)
	{
		cout << *p << ' ';
	}
}

void sort(int* a, int n) {

	//a = removeEquals(a, n);

	for (int* p = a; p < a + n; p++)
	{
		for (int* d = a; d < a + n - 1; d++)
		{
			//if (search(a, n, d)) {
			//	//int temp1 = d - a;
			//	//int temp2 = p - a;
			//	a = removeElement(a, n, d);
			//	n--;
			//	d = a;
			//	p = a;
			//	continue;
			//}
			//if (containsLetter(*d)) {
			//	// �������� ����� � ������ ������
			//	// ��� �������� ����� �� �������
			//	// ���� ��� �������� ������� � ������ �������
			//	continue;
			//}
			if (comparison(*d, *(d + 1))) {
				swap(*d, *(d + 1));
			}
		}
	}

}

bool comparison(int a, int b)
{
	a = abs(a);
	b = abs(b);

	while (a > 15) {
		a = a / 16;
	}

	while (b > 15) {
		b = b / 16;
	}

	return a < b;
}

bool search(int* array, int length, int* index)
{
	for (int* p = array; p < array + length; p++)
	{
		if (*p == *index && p != index)
		{
			return true;
		}
	}
	return false;
}

int* removeEquals(int* array, int& length)
{
	for (int* p = array; p < array + length - 1; p++)
	{
		if (search(array, length, p))
		{
			array = removeElement(array, length, p);
			p = array;
			length--;
		}
	}

	return array;
}

int* removeElement(int* array, int length, int* index)
{

	int* newArray = new int[length - 1];

	for (int* p = newArray, *d = array; p < newArray + length - 1; p++, d++)
	{
		if (d == index) {
			d++;
		}
		*p = *d;
	}

	//delete[] array;

	return newArray;
}

bool containsLetter(int number)
{
	number = abs(number);

	if (number <= 15 && number >= 10)
	{
		return true;
	}

	while (number > 15) {
		int remainder = number % 16;
		number = number / 16;

		if (remainder >= 10 && remainder <= 15 || number <= 15 && number >= 10)
		{
			return true;
		}
	}

	return false;
}

int* copyBySymbols(int* array, int length, int& newLength)
{
	int* arrayWithSymbols = new int[length];

	for (int* p = array, *d = arrayWithSymbols; p < array + length; p++)
	{
		if (containsLetter(*p))
		{
			*d = *p;	
			d++;
			newLength++;
		}
	}

	//int* finalArray = new int[newLength];

	//for (int* p = finalArray, *d = arrayWithSymbols; p < array + length; p++, d++)
	//{
	//	*p = *d;
	//}

	return arrayWithSymbols;
}

int* removeBySymbols(int* array, int& length, int* symbolArray, int symbolLength)
{
	for (int* p = array, *d = symbolArray; p < array + length; p++)
	{
		if (*p == *d)
		{
			array = removeElement(array, length, p);
			p = array;
			length--;
			d++;
		}
	}

	return array;
}

//void swap(int **a, int **b)
//{
//	int *temp = *a;
//	*a = *b;
//	*b = temp;
//}

void swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}